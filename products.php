<?php
/****************************************************
 * library of ggerman
 * license GPL 2 attached
 * contact me ggerman@gmail.com
 * ZenCart to Merchant of google
 *
 * generation of feed by txt procedure and upload the file
 *
 * Product contain the class for get products of zencart and generate txt for google merchant
 * ***************************************************/

class Product {
  public $id;
  public $title;
  public $description;
  public $google_product_category;
  public $product_type;
  public $link;
  public $image_link;
  public $condition;
  public $availability;
  public $price;
  public $sale_price;
  public $sale_price_effective_date;
  public $gtin;
  public $brand;
  public $mpn;
  public $item_group_id;
  public $gender;
  public $age_group;
  public $color;
  public $size;
  public $shipping;
  public $shipping_weigth;

  private $status;
  private $product;

  function __construct($product) {
    $this->setProduct($product);
    $this->setVariables();
  }

  private function toUrl($text){
    $text = strtolower($text);
    $text = str_replace(' ', '-', $text);
    $text = str_replace('---', '-', $text);
    $text = trim(preg_replace('/\t+/', '', $text));
    return $text;
  }
 
  public function __call($fn, array $args) {
    switch($fn) {
     case 'setVariables': 
          $this->id = $this->product['products_id'];
          $this->title = $this->setTitle($this->product['title']);
          $this->description = $this->setDescription($this->product['description']);
          $this->google_product_category = 'Electronics &gt; Communications &gt; Telephony';
          /**
           *  cat products.txt | grep product_type | uniq | wc -l
           *  tip for know how much differents products type you have in your feed file
           * */
          $this->product_type = $this->product['product_type'];
          $this->link = URL_WEBSITE."/products/".$this->toUrl($this->product['title'])."/".$this->product['products_id'];
          $this->image_link = URL_WEBSITE_IMAGES.$this->product['products_image'];
          $this->condition = 'new';
          $this->availability = 'in stock';
          $this->price = number_format($this->product['products_price'], 2);
          $this->sale_price = $this->price;
          $this->sale_price_effective_date = null;
          $this->gtin = $this->setUpc();
          $this->brand = $this->product['manufacturers_name'];
          $this->mpn = $this->setMpn();
          $this->item_group_id = $this->product['manufacturers_name'];
          $this->gender = null;
          $this->age_group = null;
          $this->color = null;
          $this->size = null;
          $this->shipping = 'US';
          $this->shipping_weigth = $this->product['products_weight'];
     break;
     case 'setTitle':
       return trim(preg_replace('/\t+/', '', $args[0]));
     break;
     case 'setDescription':
       $description = str_replace('&nbsp;', ' ', $args[0]);
       $description = trim(preg_replace('/\s\s+/', ' ', substr(strip_tags($description), 0, 140)));
       $description = trim(preg_replace('/\t+/', '', $description));
       return $description;
     break;
     case 'setMpn':

         if(!empty($this->product['products_model'])) {
            $mpn = $this->product['products_model'];
         } else {
            if($this->getStatus()){
              $mpn = 'TD100'.$this->product['products_id'];
            } else {
              $mpn = 'TD000'.$this->product['products_id'];
            }
         }
          return $mpn; 
     break;
     case 'setUpc':
          if(empty($this->product['products_upc']) || !is_numeric($this->product['products_upc'])) {
            return null;
          } else {
              return $this->product['products_upc'];
          }
     break;

    }

  }

  public function getStatus() {
    if(($this->product['products_status'] == 0) || ($this->product['products_price'] == '0.0000')) {
      return false;
    } else {
      return true;
    }
  }

  private function setProduct($product) {
    $this->product = $product;
  }

}

class Products {
  public $products = array();
  protected $product_type;

  function __construct() {
    $connect = mysql_connect(DB_SERVER, DB_SERVER_USERNAME, DB_SERVER_PASSWORD);
    mysql_select_db(DB_DATABASE);
    $products_sql = "select 
                              manufacturers.manufacturers_name,
                              products.*,
                              products_description.products_name as title,
                              products_description.products_description as description,
                              products_description.products_shortdescription as shortdescription
                            from
                              products, products_description, manufacturers
                            where (
                              (products.products_id = products_description.products_id)
                                and
                               (products.manufacturers_id = manufacturers.manufacturers_id)
                            )";
    $csql = mysql_query($products_sql);
    $row = mysql_num_rows($csql);
    $count = 0;
    while ($count < $row) {
      $register = mysql_fetch_array($csql);
      $this->getProductType($register['products_id']);
      $register['product_type'] = $this->product_type;

      switch(get_class($this)) {
        case 'GoogleFeeds': $product = new GoogleFeed($register);
        break;
      }

      $this->products[$count] = $product;
     $count++;
    }
  }
  private function getProductType($id) {
    $category_sql = "select 
                            categories_description.categories_name,
                            categories.parent_id
                            from
                              categories,
                              categories_description,
                              products_to_categories
                            where (
                              ('{$id}' = products_to_categories.products_id) 
                                and
                              (products_to_categories.categories_id = categories.categories_id)
                                and
                              (categories_description.categories_id = categories.categories_id)
                            ) GROUP BY categories_description.categories_name;";
    $csql = mysql_query($category_sql);
    $row = mysql_num_rows($csql);
    $count = 0;

    while ($count < $row) {
     $register = mysql_fetch_array($csql);
     $this->product_type = $register['categories_name'];
     $this->getCategory($register['parent_id']);
     $count++;
    }
  }

  private function getCategory($id) {
    $category_sql = "select
                            categories.parent_id,
                            categories_description.categories_name
                            from
                              categories,
                              categories_description
                            where (
                                (categories.categories_id = '{$id}')
                                  and
                                (categories.categories_id = categories_description.categories_id)
                            )";
    $csql = mysql_query($category_sql);
    $register = mysql_fetch_array($csql);

    if($register['parent_id'] != 0) {
      $this->product_type .= $register['categories_name'].", ".$this->product_type;

      $this->getCategory($register['parent_id']);
    } else {
      $this->product_type = $register['categories_name'].", ".$this->product_type;
    }
  }

  public function printing() {
    print_r($this->products);
  }

}

