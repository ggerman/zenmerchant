<?php
/****************************************************
 * library of ggerman
 * license GPL 2 attached
 * contact me ggerman@gmail.com
 * ZenCart to Merchant of google
 *
 * generation of feed by txt procedure and upload the file
 * ***************************************************/
require_once 'configuration.php';

function __autoload($class_name) {
  require_once strtolower($class_name).'.php';
}

$products = new GoogleFeeds();
$products->createFeed();

$upload = new Ftp(FTP_HOSTS, FTP_USER, FTP_PASSWORD, '/home/ggerman/www/merchants/feed.txt');
echo ($upload ? "[ OK ]\n" : "[ ERROR ]\n");
